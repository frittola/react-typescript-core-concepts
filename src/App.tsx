import React, { lazy, Suspense } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import { DemoUseEffect } from './views/13-hooks-examples/Demo50UseEffects';
import { DemoUseEffectOptimized } from './views/13-hooks-examples/Demo51UseEffectsOptimizedRenders';
import { SimpleFormDemo } from './views/6-forms/Demo10FormValidations';
import { DemoToggable } from './views/2-reusable-components/DemoComponent3Toggable';
import { DemoComponentPanel } from './views/2-reusable-components/DemoComponent2Panel';
import { DemoListUncontrolledForm } from './views/14-crud/step1/DemoListUncontrolledForm';
import { DemoListFetch } from './views/14-crud/step2/DemoListFetch';
import { DemoListFetchCustomHooks } from './views/14-crud/step3/DemoListCustomHooks';
import { DemoListFetchUseReducer } from './views/14-crud/step4/DemoListFetchUseReducer';
import DemoUseMemo2 from './views/13-hooks-examples/Demo62UseMemoFibonacci';
import { DemoUseMemo } from './views/13-hooks-examples/Demo61UseMemo';
import { Empty } from './views/empty';
import { DemoHelloComponents } from './views/1-fundamentals/Demo1HelloComponents';
import { DemoInlineStyling } from './views/1-fundamentals/Demo4InlineStyling';
import { DemoCSSClass } from './views/1-fundamentals/Demo2CSSClass';
import { DemoCSSClassnames } from './views/1-fundamentals/Demo3CSSClassnamesUtil';
import { DemoChildren } from './views/1-fundamentals/Demo5Children';
import { DemoMouseEvents } from './views/1-fundamentals/Demo6MouseEvents';
import { DemoUnControlledForm } from './views/6-forms/Demo8UnControlledForm';
import { DemoControlledForms } from './views/6-forms/Demo9ControlledForms';
import { DemoPropsCallback } from './views/1-fundamentals/Demo7PropsAsCallback';
import { DemoSimpleList1 } from './views/4-lists/DemoList1';
import { DemoSimpleList2 } from './views/4-lists/DemoList2';
import { DemoMapQuest } from './views/2-reusable-components/DemoComponent1MapQuest';
import { TabBarDemo } from './views/2-reusable-components/DemoComponent4TabBar';
import { DemoHelloClass } from './views/20_various/Demo1HelloClass';
import { DemoComponentBased1 } from './views/9-approccio-component-based/example1A-useState/DemoComponentBased1_useState';
import { DemoComponentBased2 } from './views/9-approccio-component-based/example2A-useReducer/DemoComponentBased2';
import { TabBarDemoWithGenericsAndCustomProp } from './views/2-reusable-components/DemoComponent5TabBarGenericsCustomProp';
import { DemoComponentBased1CustomHook } from './views/9-approccio-component-based/example1B-custom-hook/DemoComponentBased1B_customHook';
import { DemoComponentBased2CustomHook } from './views/9-approccio-component-based/example2B-custom-hook/DemoComponentBased2B';
import { Demo8simpleHooks } from './views/1-fundamentals/Demo8useStateuseEffectHooks';
import { ProductDetails } from './views/30_router/ProductDetails';
import { DemoUseContext } from './views/13-hooks-examples/Demo71UseContext';
import { DemoUseContextDynamic } from './views/13-hooks-examples/Demo72UseContextDynamic';
import { Demo1UseReducer } from './views/13-hooks-examples/Demo80UseReducer';
import { Demo2UseReducerCustomHook } from './views/13-hooks-examples/Demo81UseReducerCustomHook';
import { Demo10UseState } from './views/13-hooks-examples/Demo10UseState';
import { Demo11UseStateCustomHook } from './views/13-hooks-examples/Demo11UseStateCustomHook';
import { DemoHelloReact } from './views/1-fundamentals/Demo0HelloReact';
import { Demo1UseState } from './views/3-hooks-intro/Demo01-Counter';
import { Demo01CustomHooks } from './views/11-reusable-custom-hooks/Demo01CustomHooks';

const ExampleMenuLazy = lazy(() => import('./views/core/ExampleMenu'));

const App: React.FC = () => {
  return (
    <div className="mt-3">
      <Router>
        <Suspense fallback={<div>Loading...</div>}>

          <Switch>

            {/*MENU*/}
            <Route path="/" exact component={ExampleMenuLazy}/>

            {/*FUNDAMENTALS*/}
            <Route path="/empty" component={Empty}/>
            <Route path="/fundamentals/hello-react" component={DemoHelloReact}/>
            <Route path="/fundamentals/hello-react-props" component={DemoHelloComponents}/>
            <Route path="/fundamentals/hello-react-components" component={DemoHelloComponents}/>
            <Route path="/fundamentals/css-class" component={DemoCSSClass}/>
            <Route path="/fundamentals/css-classnames" component={DemoCSSClassnames}/>
            <Route path="/fundamentals/inline-style" component={DemoInlineStyling}/>
            <Route path="/fundamentals/children" component={DemoChildren}/>
            <Route path="/fundamentals/props-callback" component={DemoPropsCallback}/>
            <Route path="/fundamentals/mouseevents" component={DemoMouseEvents}/>
            <Route path="/fundamentals/useStateuseEffect" component={Demo8simpleHooks}/>

            {/*HOOKS INTRO*/}
            <Route path="/hooks-intro/use-state" component={Demo1UseState}/>

            {/*REUSABLE COMPONENTS*/}
            <Route path="/components/static-map-quest" component={DemoMapQuest}/>
            <Route path="/components/panel" component={DemoComponentPanel}/>
            <Route path="/components/toggable" component={DemoToggable}/>
            <Route path="/components/tabbar-generics" component={TabBarDemo}/>
            <Route path="/components/tabbar-generics-custom-prop" component={TabBarDemoWithGenericsAndCustomProp}/>

            {/*LIST*/}
            <Route path="/list/simple" component={DemoSimpleList1}/>
            <Route path="/list/useState" component={DemoSimpleList2}/>

            {/*FORM*/}
            <Route path="/forms/uncontrolled-forms" component={DemoUnControlledForm}/>
            <Route path="/forms/controlled-forms" component={DemoControlledForms}/>
            <Route path="/forms/validations" component={SimpleFormDemo}/>
            {/*COMPONENT BASED APPROACH*/}
            <Route path="/component-based/demo1A-usestate" component={DemoComponentBased1}/>
            <Route path="/component-based/demo1B-customhooks" component={DemoComponentBased1CustomHook}/>
            <Route path="/component-based/demo2A-usereducer" component={DemoComponentBased2}/>
            <Route path="/component-based/demo2B-customhooks" component={DemoComponentBased2CustomHook}/>

            {/*HOOKS INTRO*/}
            <Route path="/hooks-examples/demo-use-state" exact component={Demo10UseState}/>
            <Route path="/hooks-examples/demo-use-state-custom-hook" exact component={Demo11UseStateCustomHook}/>
            <Route path="/hooks-examples/demo-use-context" exact component={DemoUseContext}/>
            <Route path="/hooks-examples/demo-use-context-dynamic" exact component={DemoUseContextDynamic}/>
            <Route path="/hooks-examples/demo-use-effect" exact component={DemoUseEffect}/>
            <Route path="/hooks-examples/demo-use-effect-optimized" exact component={DemoUseEffectOptimized}/>
            <Route path="/hooks-examples/demo-use-reducer-simple" exact component={Demo1UseReducer}/>
            <Route path="/hooks-examples/demo-use-reducer-custom-hook" exact component={Demo2UseReducerCustomHook}/>
            <Route path="/hooks-examples/demo-use-memo1" exact component={DemoUseMemo}/>
            <Route path="/hooks-examples/demo-use-memo2" exact component={DemoUseMemo2}/>

            <Route path="/reusable-hooks" exact component={Demo01CustomHooks}/>

            {/*CRUD*/}
            <Route path="/crud/list-and-uncontrolled-forms" component={DemoListUncontrolledForm}/>
            <Route path="/crud/list-fetch" component={DemoListFetch}/>
            <Route path="/crud/list-fetch-custom-hooks" component={DemoListFetchCustomHooks}/>
            <Route path="/crud/list-fetch-use-reducer" component={DemoListFetchUseReducer}/>

            {/*VARIOUS*/}
            <Route path="/class/hello" component={DemoHelloClass}/>

            {/*ROUTE WITH PARAMS*/}
            <Route path="/products/:id" component={ProductDetails}/>
          </Switch>
        </Suspense>

        <div className="text-center mt-5">
          <hr/>
          <Link to="/">Back to Menu</Link>
        </div>
      </Router>
    </div>
  )
}

export default App;
