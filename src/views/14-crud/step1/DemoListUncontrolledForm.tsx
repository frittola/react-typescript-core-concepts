import React, {
  KeyboardEvent,
  useRef
} from 'react';

type Item = {
  id: number,
  name: string
}

export const DemoListUncontrolledForm: React.FC = () => {
  const inputEl = useRef<HTMLInputElement>(null);

  const [list, setList] = React.useState([
    { id: 10, name: 'One' }, { id: 20, name: 'two' }
  ]);

  const deleteItem = (itemToDelete: any) => {
    setList(prevState => prevState.filter(item => item.id !== itemToDelete.id))
  };

  const addItem = (event: KeyboardEvent<HTMLInputElement>) => {
    const target = inputEl.current;

    if (event.key === 'Enter' && target) {
      const newItem: Item =  {
        id: Date.now(),
        name: target.value || ''
      };
      setList(prevState => [...prevState, newItem]);
      target.value = '';
    }
  };

  return (
    <div>
      <h3>SIMPLE LIST: #{list.length} Items</h3>

      {/*form*/}
      <input ref={inputEl} type="text" onKeyPress={addItem}/>

      {/*list*/}
      {
        list.map((item: Item, index: number) => {
          const { id, name } = item;
          return (
            <li className="list-group-item" key={item.id}>
              {index+1} - {name} ({ id })
              <div className="pull-right">
                <i className="fa fa-trash" onClick={() => deleteItem(item)} />
              </div>
            </li>
          )
        })
      }
    </div>
  );
}

