import React from "react"
import classNames from 'classnames';

export const DemoCSSClassnames: React.FC = () => {
  return (
    <div className="example-box-centered small">
      <Panel
        title="hello react"
        text="How are you?"
        header="dark"
      />
    </div>
  )
};

// =========================================
// PANEL SIMPLE COMPONENT WITH "classNames" util
// =========================================

interface PanelProps {
  title?: string;
  text?: string;
  header?: 'dark' | 'light'
}

export const Panel: React.FC<PanelProps> = props => {
  const cssHeader = classNames(
    'card-header',
    { 'bg-dark text-white': props.header === 'dark'},
    { 'bg-light': props.header === 'light'},
  );

  return (
    <div className="card">
      { props.title && <div className={cssHeader}>{props.title}</div> }
      <div className="card-body">{props.text}</div>
    </div>
  )
};
