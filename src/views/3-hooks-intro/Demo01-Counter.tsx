import React, { useEffect } from 'react';


export const Demo1UseState: React.FC = () => {
  const [id, setId] = React.useState<number>(1);

  return  <h1>
    <UserDetails id={id} />
    <button onClick={() => setId(1)}>1</button>
    <button onClick={() => setId(2)}>2</button>
    <button onClick={() => setId(3)}>3</button>
  </h1>
};

// ====================================
// User Details Component
// ====================================

interface UserDetailsProps {
  id: number;
}
interface User {
  name: string;
  email: string;
}


export const UserDetails: React.FC<UserDetailsProps> = props => {
  const [user, setUser] = React.useState<User | null>(null);

  // Version with Promise
  useEffect(() => {
    fetch(`https://jsonplaceholder.typicode.com/users/${props.id}`, { method: 'GET' })
      .then(res => res.json())
      .then(res => setUser(res))
  },[props.id]); // <== IMPORTANT to avoid infinite renders

  // Version with async await
  useEffect(() => {
    (async function() {
      const response = await fetch(`https://jsonplaceholder.typicode.com/users/${props.id}`, { method: 'GET' });
      setUser(await response.json())
    })();
  },[props.id]);

  return user && <div>
    {user.name}
    {user.email}
  </div>
};

