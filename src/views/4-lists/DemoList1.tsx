import React from 'react'
import { User } from './model/user';
import { data } from './data/mock';

export const DemoSimpleList1: React.FC = () => {

  const getTotal = () => {
    return data.reduce((acc: number, item: User) => {
      return acc + item.tweets;
    }, 0)
  };


  return (
    <div>
      {
        data.map((item, index) => {
          return (
            <li key={item.id} className="list-group-item">
              {index + 1}. {item.name}
              <div className="pull-right">
                <div className="badge badge-dark pull-right">
                  {item.tweets}
                </div>
              </div>
            </li>
          )
        })
      }

      <div className=" mr-3 text-center">
        <strong className="badge badge-info">
          {getTotal()} TWEETS
        </strong>
      </div>
      <br/>

    </div>
  )
};
