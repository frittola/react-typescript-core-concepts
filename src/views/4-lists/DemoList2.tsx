import React, { useEffect } from 'react'
import { User } from './model/user';

const API_URL = 'http://localhost:3001/tweets';

export const DemoSimpleList2: React.FC = () => {
  const [users, setUsers] = React.useState<User[]>([]);
  const [error, setError] = React.useState<boolean>(false);

  useEffect(() => {
    (async function () {
      try {
        const response: Response = await fetch(API_URL, { method: 'GET' });
        // if there is a response
        // try it: http://localhost:3001/XYZ
        if (response.ok) {
          const result: User[] = await response.json();
          setUsers(result);
          setError(false);
        } else {
          setError(true);
        }
      } catch (e) {
        //  url is wrong
        // try it: http://www.xyz.com
        setError(true);
        console.log('error', e)
      }

    })()
  }, []);

  const deleteUser = async (user: User) => {
    try {
      const response: Response = await fetch(`${API_URL}/${user.id}`, { method: 'DELETE' });
      if (response.ok) {
        setUsers(users.filter(u => u.id !== user.id));
        setError(false);
      } else {
        setError(true);
      }
    } catch (e) {
      setError(true);
    }

  };

  const getTotal = () => {
    return users.reduce((acc: number, item: User) => {
      return acc + item.tweets;
    }, 0)
  };

  return (
    <div className="example-box-centered small">
      {
        users.map((item, index) => {
          return <li key={item.id} className="list-group-item">
            {index + 1}. {item.name}
            <div className="pull-right">
              <div className="badge badge-dark mr-2">{item.tweets}</div>
              <i className="fa fa-trash" onClick={() => deleteUser(item)} />
            </div>
          </li>
        })
      }

      {
        error ?
          <div className="alert alert-danger">Server error</div> :
          <div className="pull-right mr-3">
            <strong>TOTAL: {getTotal()}</strong>
          </div>
      }
      <br/>

    </div>
  )
};
