import React, { useContext } from 'react';

const themes = {
  light: {
    foreground: "#000000",
    background: "#eeeeee"
  },
  dark: {
    foreground: "#ffffff",
    background: "#222222"
  }
};

// Create type from initial value (dark or light is the same)
type ThemeProps = typeof themes.dark;

// Define context with initial light
const ThemeContext = React.createContext<ThemeProps>(themes.light);

// root component: level 0
export const DemoUseContext: React.FC  = () => {
  return (
    <ThemeContext.Provider value={themes.dark}>
      <Toolbar />
    </ThemeContext.Provider>
  );
};

// middle component: level 1
const Toolbar: React.FC = () => {
  return (
    <div>
      <ThemedButton label="Button 1" />
      <ThemedButton label="Button 2" />
    </div>
  );
};

// last (leaf) component: level 2
const ThemedButton: React.FC<{ label: string}> = ({label}) => {
  const theme = useContext(ThemeContext);

  const css = {
    background: theme.background,
    color: theme.foreground
  };

  return (
    <button style={css}>
      {label}
    </button>
  );
};
