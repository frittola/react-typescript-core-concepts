import React, { useEffect } from 'react';

type Item = {
  id: number,
  name: string
}

export const DemoUseEffect: React.FC = () => {
  const [list, setList] = React.useState([
    { id: 1, name: 'One' }, { id: 2, name: 'two' }
  ]);

  // Similar to componentDidMount and componentDidUpdate:
  useEffect(() => {
    // Update the document title using the browser API
    document.title = `There are ${list.length} items`;

    return () => {
      document.title = ``;
    };
  });

  const setListHandler = (itemToDelete: any) => {
    setList(
      items => items.filter(item => item.id !== itemToDelete.id)
    )
  };

  return (
    <>

      <h3>{list.length} Products</h3>

      <h6>UseState & UseEffects</h6>
      {
        list.map((item: Item, index: number) => {
          return (
            <li className="list-group-item" key={index}>
              {index+1} - {item.name}
              <i className="fa fa-trash" onClick={() => setListHandler(item)} />
            </li>
          )
        })
      }
    </>
  );
}
