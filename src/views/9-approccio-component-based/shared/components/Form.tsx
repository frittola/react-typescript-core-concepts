import React, { FormEvent, useEffect, useState } from 'react';
import { Product } from '../model/product';
import classNames from 'classnames';

interface FormProps {
  active: Product
  onReset: () => void;
  onAdd: (u: Product) => void;
}

export const Form: React.FC<FormProps> = (props) => {
  const [formState, setFormState] = useState<Product>({ name: '', price: 0});

  useEffect(() => {
    setFormState(props.active)
  }, [props.active]);

  const onChange = (event: FormEvent<HTMLInputElement>) => {
    const target = event.currentTarget;
    setFormState(state => ({
      ...state,
      [target.name]: target.type === 'number' ? +target.value : target.value
    }));
  };

  const submit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    props.onAdd(formState);
  };

  const nameIsValid = formState.name && formState.name.length > 0;
  const priceIsValid = formState.price > 0;
  const valid =  nameIsValid && priceIsValid;

  return (
    <form onSubmit={submit}>
      <input
        type="text"
        name="name"
        className={
          classNames('form-control', { 'is-invalid': !nameIsValid })
        }
        value={formState.name}
        placeholder="Product"
        onChange={onChange}
      />

      <input
        type="number"
        name="price"
        className={
          classNames('form-control', { 'is-invalid': !priceIsValid })
        }
        value={formState.price}
        placeholder="Price"
        onChange={onChange}
      />

      <div className="btn-group">
        <button
          className="btn btn-outline-primary"
          disabled={!props.active.id}
          type="button"
          onClick={props.onReset}
        >CLEAN</button>

        <button
          type="submit"
          className="btn btn-primary"
          disabled={!valid}
        >
          {props.active.id ? 'EDIT' : 'ADD'}
        </button>
      </div>
    </form>
  )
};
