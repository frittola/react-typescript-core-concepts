import { Product } from '../../shared/model/product';


type Action =
  { type: 'GET', payload: Product[] } |
  { type: 'DELETE', payload: Product } |
  { type: 'ADD', payload: Product } |
  { type: 'EDIT', payload: Product };


export function productsReducer(state: Product[], action: Action) {
  switch (action.type) {
    case 'GET':
      return action.payload;

    case 'ADD':
      return [...state, action.payload];

    case 'DELETE':
      return state.filter(u => u.id !== action.payload.id);

    case 'EDIT':
      return state.map(u => {
        return u.id === action.payload.id ? action.payload : u;
      });
  }
  return state;
}
