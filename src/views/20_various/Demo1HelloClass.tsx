import React, { useEffect, useState } from 'react';

export const DemoHelloClass: React.FC = () => {
  let [count, setCount] = useState<number>(0);

  useEffect(() => {
    const id = setInterval(() => {
      setCount(++count);
    }, 1000);

    return () => {
      clearInterval(id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <Panel count={count}>ciao</Panel>
}



interface Props {
  count: number;
}
interface DemoState {
  internalCounter: number;
}
// type State = DemoState & ReturnType<typeof transformPropsToState>;

class Panel extends React.PureComponent<Props, DemoState> {
  constructor(props: Props) {
    super(props);
    this.state = {
      internalCounter: 0
    };
  }

  componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<DemoState>, snapshot?: any): void {
    console.log(prevProps, this.props)
  }

  componentDidMount(): void {
    console.log('did mount')
  }

  static getDerivedStateFromProps(props: Props, state: DemoState) {
    if (props.count !== state.internalCounter) {
      return {
        ...state,
        internalCounter: props.count * 10
      }
    }
  }

  componentWillUnmount(): void {
    console.log('willUnmount')
  }

  render() {
    return <div>
      <div>Panel Counter Prop:{this.props.count}</div>
      <div>Panel Counter Internal State: {this.state.internalCounter}</div>
    </div>
  }
}
